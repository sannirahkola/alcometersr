import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  //Luodaan muuttujia
  private genders = [];
  private hours = [];
  private bottles = [];
  private weight: number;
  private gender: string;
  private hour: string;
  private bottle: string;
  private promilles: number;

  private grams: number;
  private gramsleft: number;
  private burning: number;

  constructor() {}

  ngOnInit() {

    //Laitetaan arvoja valikkoihin
    this.genders.push('Male');
    this.genders.push('Female');


    this.hours.push('1');
    this.hours.push('2');
    this.hours.push('3');
    this.hours.push('4');
    this.hours.push('5');

    this.bottles.push('1');
    this.bottles.push('2');
    this.bottles.push('3');
    this.bottles.push('4');
    this.bottles.push('5');

    //Säädetään oletusarvot, jotka näkyvät sivulla
    this.weight = 89;
    this.gender = 'Male';
    this.hour = '1';
    this.bottle = '3';

  }

  //Täällä lasketaan promillemäärät riippuen valitusta ajasta, sukupuolesta ja pullomäärästä
  private calculate() {

    let litres = 0;
    let time = 0;

    //Laitetaan litramäärä riippuen valittujen pullojen määrästä
    switch (this.bottle) {
      case '1':
        litres = 0.33;
        break;
      case '2':
        litres = 2 * 0.33;
          break;
      case '3':
        litres = 3 * 0.33;
          break;
      case '4':
        litres = 4 * 0.33;
        break;
      case '5':
        litres = 5 * 0.33;
        break;
    }

    //Laitetaan tuntimäärä riippuen valitusta tunnista
    switch (this.hour) {
      case '1':
        time = 1;
        break;
      case '2':
        time = 2;
          break;
      case '3':
        time = 3;
          break;
      case '4':
        time = 4;
        break;
      case '5':
        time = 5;
        break;
    }

    //Laskutoimituksia
    this.grams = litres * 8 * 4.5;
    this.burning = this.weight / 10;
    this.gramsleft = this.grams - (this.burning * time);

    if (this.gender === 'Male') {
      this.promilles = this.gramsleft / (this.weight * 0.7);

    }
    else {
      this.promilles = this.gramsleft / (this.weight * 0.6);
    }
  }

}